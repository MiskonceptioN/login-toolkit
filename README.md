# README #

So, I'm not sure about licensing and etc but this code is based off of a fantastic series of tutorials by [Codecourse](https://www.youtube.com/channel/UCpOIUW62tnJTtpWFABxWZ8g). If you're new to OOP, login systems, sessions, etc definitely watch through all of the tutorial videos!
Link: http://www.youtube.com/playlist?list=PLfdtiltiRHWF5Rhuk7k4UAU1_yLAZzhWc

### What is this? ###

Quite simply this is a basis from which to develop a decent user login system which manages sessions, prevents cross site request forgery (XSRF / CSRF), etc.
Over time I am planning on developing this as my own, adding, removing and changing features as necessary.

### What is this not? ###

* Pretty
* 100% secure
* Suitable for every application

### Contributing ###

Please feel free to contribute by making a new branch, adding well commented code and frequent commits. Once you're done, push a branch to the repo and submit a pull request.